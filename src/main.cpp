//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

#include <FreeRTOS.h>
#include <task.h>

#include <segger/SEGGER_SYSVIEW.h>

// ----------------------------------------------------------------------------
//
// Standalone STM32F4 empty sample (trace via ITM).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the ITM output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//
// ----- main() ---------------------------------------------------------------

extern unsigned int _Heap_Begin;
extern unsigned int _Heap_Limit;

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

int
main(int argc, char* argv[])
{
	/* define the heap regions for the RTOS */
	HeapRegion_t xhregions[] =
	{
			{NULL, 0},
			{NULL, 0}
	};
	xhregions[0].pucStartAddress = (uint8_t *)&_Heap_Begin;
	xhregions[0].xSizeInBytes = (size_t)((size_t)&_Heap_Limit - (size_t)&_Heap_Begin);
	vPortDefineHeapRegions(xhregions);

	/* segger debug utils init */
	SEGGER_SYSVIEW_Conf();

	/* start the rtos */
	vTaskStartScheduler();

	// Infinite loop
	// Should never return
	while (1)
	{
	}
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
